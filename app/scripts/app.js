'use strict';

/**
 * @ngdoc overview
 * @name myProjectApp
 * @description
 * # myProjectApp
 *
 * Main module of the application.
 */

var app = angular.module('myProjectApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngTouch',
    'ui.grid',
    'ui.grid.selection',
    'ui.grid.pagination',
    'restangular',
    'learnangular.services'
]);

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 'RestangularProvider', function(
    $stateProvider, $urlRouterProvider, $httpProvider, RestangularProvider) {
    $stateProvider
        .state('index', {
            url: '/index',
            templateUrl: 'views/main.html'
        }).state('addpupil', {
            url: '/addpupil',
            templateUrl: 'views/add.html'
        }).state('editpupil', {
            url: '/addpupil/:learnerid',
            templateUrl: 'views/add.html'
        }).state('list', {
            url: '/list',
            templateUrl: 'views/list.html'
        });

        $urlRouterProvider.otherwise('/list');

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    RestangularProvider.setBaseUrl("http://192.168.50.137:8080/simple-service-webapp");
    RestangularProvider.setDefaultHeaders({
        'Content-Type': 'application/json'
    });
    RestangularProvider.setRestangularFields({
        id: 'LearnerId'
    });
}]);