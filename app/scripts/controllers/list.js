'use strict';

/**
 * @ngdoc function
 * @name myProjectApp.controller:ListCtrl
 * @description
 * # MainCtrl
 * Data list controller to display data on datagrid
 */

app.controller('ListCtrl', ['$scope','$http','$interval'
	                       ,'uiGridConstants','groupsService','learnersService'
	                       ,function($scope,$http,$interval,uiGridConstants
	                       	        ,groupsService,learnersService){

	$scope.gridOptions = {
			enableRowSelection: true,
			enableSelectAll: true,
			selectionRowHeaderWidth: 35,
			rowHeight: 35,
			showGridFooter:false,
			multiSelect: true,
			enablePaginationControls:true,
			paginationPageSizes: [9, 15, 20],
    	    paginationPageSize: 9
			};

	$scope.gridOptions.columnDefs = [
			{ name: 'LearnerId',displayName:'Learner ID'},
			{ name: 'GroupId',displayName:'Group ID' },
			{ name: 'LearnerName', displayName: 'Learner Name'}
			];

	$scope.gridOptions.onRegisterApi = function( gridApi ) {
		   $scope.gridApi = gridApi;
		 };

    $scope.learnersList = [];
	$scope.groupList = [];

	$scope.keywordContent = "";
	$scope.keywordGroupID = "";

	$scope.gridOptions.data =  $scope.learnersList;

	var updateWhenNoSelectWarn = "Please select only one row to continue!";
	var deleteWhenNoSelectWarn = "Please select at least one row to continue!";
	
	//We watch the collection, but it's not a good idea in real.
	//manually do this may be better.
	$scope.$watchCollection('learnersList', function(newLL, oldLL) {
		  	$scope.gridOptions.data =  $scope.learnersList;
		 });

	var getDataList = function(){
		learnersService.all().then(function(data){
			$scope.learnersList = data.learnerList.learner;
		});
	}

	getDataList();
       
    $scope.query = function(){
    	if($scope.keywordContent!="")
    	{
    		learnersService.find($scope.keywordContent).then(function(data){
                if(data!=undefined)
                {
        			var queryData = [{LearnerId:data.LearnerId,GroupId:data.GroupId,LearnerName:data.LearnerName}];
        			$scope.learnersList = queryData;
                }
                else
                {
                    showDialog("Please Notify!","Can't find any learners you want!");
                }
    		});
    	}
        else
        {
            showDialog("Please Notify!","Please inpput LearnerId to query!");
        }
    }

    $scope.clearQuery = function(){
    	if($scope.keywordContent!="")
    	{
			$scope.keywordContent = "";
		    getDataList();
    	}
    }

    $scope.checkEdit =  function(){
    	var flag = checkSelectionAndWarn(false,updateWhenNoSelectWarn);
    	if(flag)
    	{
    		var selectedObject = $scope.gridApi.selection.getSelectedRows();
			window.location.href="#/addpupil/"+selectedObject[0].LearnerId;
    	}
    }

    $scope.delete = function(){
    	var flag = checkSelectionAndWarn(true,deleteWhenNoSelectWarn);
    	if(flag)
    	{
    		var selectedObjects = $scope.gridApi.selection.getSelectedRows();
    		var selectedObjectsCount = selectedObjects.length;
    		for(var i=0;i<selectedObjectsCount;i++)
    		{
    			var learnerId = selectedObjects[i].LearnerId;
    			learnersService.deleteLearn(learnerId).then(function(data){
                    if(data==0)
                    {
    				 getDataList();
                    }
                    else
                    {
                        showDialog("Delete error!","Failed to delete the learner, please retry!");
                    }
   	 			});
    		}
    	}
    }

    $scope.refresh = function(){
        getDataList();
        showDialog("Refreshed OK","Refreshed OK now!",1000);
    }

    $scope.help = function(){
         showDialog("About This Project","This project is our first daemon project, we are on it and we love it!",3000);
    }

    var showDialog = function(title,content,time){
        var d = dialog({title: title,content: content});
        d.show();
        if(time!=null && time!=undefined)
        {
            setTimeout(function () {
                d.close().remove();
            }, time);
        }
    }

    var checkSelectionAndWarn = function(allowMultipleLearnersSelected,warnInfo)
    {
    	var selectedObjects = $scope.gridApi.selection.getSelectedRows();
    	var selectedObjectCount = selectedObjects.length;
    	if(allowMultipleLearnersSelected)
    	{
    		if(selectedObjectCount <= 0)
    		{
    			showDialog("Please Notify!",warnInfo);
    			return false;
    		}
    		else
    		{
    			return true;
    		}
    	}
    	else
    	{
			if(selectedObjectCount <=0 || selectedObjectCount > 1)
			{
				showDialog("Please Notify!",warnInfo);
				return false;
			}    		
			else
			{
				return true;
			}
    	}
    }

}]);